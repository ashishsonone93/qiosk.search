//run : mocha unit_tests/api/ --watch
var sinon = require('sinon');
var RSVP = require('rsvp');
var elasticsearch = require('elasticsearch');
var express = require('express');

var chai = require('chai');
var chaiHttp = require('chai-http');
chai.use(chaiHttp);

var expect = chai.expect;

var searchApi = require('../../api/search_api');

describe('search api : router init', function() {
  var client = {
    get : function(){}
  };
  
  beforeEach(function() {
    sinon.stub(elasticsearch, 'Client', function(config){
      return client;
    });
  });

  afterEach(function() {
    elasticsearch.Client.restore();
  });
  
  it('router init', function() {
    var router = searchApi.router('<server>', '<index>');
    expect(elasticsearch.Client.getCall(0).args[0].host).to.equal('<server>');
  });
  
});

describe('search api : get item by id', function() {
  var client = {
    get : function(){}
  };
  var app;
  
  before(function(){
    sinon.stub(elasticsearch, 'Client', function(config){
      return client;
    });
    var router = searchApi.router('<server>', '<index>');
    app = express();
    app.use('/', router);
  });
  
  beforeEach(function() {
    sinon.stub(client, 'get', function(params){
      //_id with 10 in it won't be found : e.g artcle-10, user-10
      //console.log('client.get called with %j', params);
      if(params.id.indexOf('10') >= 0){
        return RSVP.reject({status : 404});
      }
      else{
        var hit = {
          _source : {
            id : params.id,
            property : 'property' 
          }
        };
        return RSVP.resolve(hit);
      }
    });
  });

  afterEach(function() {
    client.get.restore();
  });
  
  after(function(){
    elasticsearch.Client.restore();
  });
  
  it('get user by id FOUND', function(done) {
    chai.request(app)
    .get('/users/user-33')
    .end(function(err, res){
      //console.log('hiya %j %j', err, res);
      expect(client.get.getCall(0).args[0]).to.deep.equal({
        index : '<index>',
        type : 'user',
        id : 'user-33'
      });
      expect(res).to.have.status(200);
      expect(JSON.parse(res.text)._id).to.be.equal('user-33');
      done();
    });
  });
  
  it('get user by id NOT FOUND', function(done) {
    var router = searchApi.router('<server>', '<index>');
    var app = express();
    app.use('/', router);
    //app.listen(8999);
    
    chai.request(app)
    .get('/users/user-10')
    .end(function(err, res){
      //console.log('hiya %j %j', err, res);
      expect(client.get.getCall(0).args[0]).to.deep.equal({
        index : '<index>',
        type : 'user',
        id : 'user-10'
      });
      expect(res).to.have.status(404);
      expect(JSON.parse(res.text).error).to.be.equal('NOT_FOUND');
      done();
    });
  });
});

describe('search api : search by phrase', function() {
  var client = {
    search : function(){}
  };
  var app;
  
  before(function(){
    sinon.stub(elasticsearch, 'Client', function(config){
      return client;
    });
    var router = searchApi.router('<server>', '<index>');
    app = express();
    app.use('/', router);
  });
  
  beforeEach(function() {
    sinon.stub(client, 'search', function(params){
      var hit1 = {
        _score : 2,
        _source : {
          id : 'sam',
          property : 'property' 
        }
      };
      
      var hit2 = {
        _score : 1,
        _source : {
          id : 'killua',
          property : 'property' 
        }
      };
      
      var result = {
        hits : {
          total : 23,
          hits : [
            hit1,
            hit2
          ]
        }
      };
      return RSVP.resolve(result);
    });
  });

  afterEach(function() {
    client.search.restore();
  });
  
  after(function(){
    elasticsearch.Client.restore();
  });
  
  it('search user : ashish - non-zero results', function(done) {
    chai.request(app)
    .get('/users/?q=ashish&limit=2')
    .end(function(err, res){
      //console.log('hiya %j %j', err, res);
      //check if elasticsearch(client) was called with correct parameters
      expect(client.search.getCall(0).args[0].index).to.equal('<index>');
      expect(client.search.getCall(0).args[0].type).to.equal('user');
      var reqQuery = client.search.getCall(0).args[0].body; 
      expect(reqQuery.size).to.equal(2);
      expect(reqQuery.query.bool.must.multi_match.query).to.equal('ashish');
      expect(reqQuery.query.bool.must.multi_match.fields).to.deep.equal(["name.user_raw^2", "user.user_autocomplete", "description.user_english", "description.user_autocomplete"]);

      //check response
      expect(res).to.have.status(200);
      var resJson = JSON.parse(res.text);
      expect(resJson[0]._score).to.be.equal(2);
      expect(resJson[0]._id).to.be.equal('sam');
      expect(resJson[0].property).to.be.equal('property');
      done();
    });
  });
  
});