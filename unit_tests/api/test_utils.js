var chai = require('chai');
var expect = chai.expect;

var utils = require('../../api/utils/utils');

describe('utils.searchHitToResult', function() {
  it('handle general type', function() {
    var hit = {
      _score : 2.3,
      _source : {
        name : 'Ashish Sonone',
        id : 'ashishsonone'
      }
    };
    var item = utils.searchHitToResult('random')(hit);
    expect(item).to.deep.equal({
      _score : 2.3,
      name : 'Ashish Sonone',
      _id : 'ashishsonone'
    });
  });
  
  it('handle user type - isInfluencer 1', function() {
    var hit = {
      _score : 2.3,
      _source : {
        name : 'Ashish Sonone',
        id : 'ashishsonone',
        interests : ['a', 'b', 'c'],
        isInfluencer : 1
      }
    };
    var item = utils.searchHitToResult('user')(hit);
    expect(item).to.deep.equal({
      _score : 2.3,
      name : 'Ashish Sonone',
      _id : 'ashishsonone',
      interests : ['a', 'b', 'c'],
      isInfluencer : true
    });
  });
  
  it('handle user type - isInfluencer 0', function() {
    var hit = {
      _score : 2.3,
      _source : {
        name : 'Ashish Sonone',
        id : 'ashishsonone',
        interests : ['a', 'b', 'c'],
        isInfluencer : 0
      }
    };
    var item = utils.searchHitToResult('user')(hit);
    expect(item).to.deep.equal({
      _score : 2.3,
      name : 'Ashish Sonone',
      _id : 'ashishsonone',
      interests : ['a', 'b', 'c'],
      isInfluencer : false
    });
  });
  
  it('handle article type', function() {
    var hit = {
      _score : 2.3,
      _source : {
        id : 'article444',
        title : 'This is the next breakthrough',
        content : '<h1> ha ha <h1>',
        tags : ['a', 'b', 'c'],
        '#likes' : 0,
        '#notes' : 34,
        '#shares' : 9
      }
    };
    var item = utils.searchHitToResult('article')(hit);
    expect(item).to.deep.equal({
      _score : 2.3,
      _id : 'article444',
      title : 'This is the next breakthrough',
      content : '<h1> ha ha <h1>',
      tags : ['a', 'b', 'c'],
    });
  });
  
});