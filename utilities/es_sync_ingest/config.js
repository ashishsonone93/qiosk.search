
var reqEnvVariables = ['ES_SERVER', 'API_SERVER', 'BULK_LIMIT', 'ES_INDEX'];
if(!(process.env.ES_SERVER && process.env.API_SERVER && process.env.BULK_LIMIT && process.env.ES_INDEX)){
  console.log('required env variables set : %j', reqEnvVariables);
  process.exit(0);
}

module.exports = {
  ES_SERVER : process.env.ES_SERVER, // 'http://localhost:9200',
  API_SERVER : process.env.API_SERVER, // 'http://localhost:8003',
  BULK_LIMIT : process.env.BULK_LIMIT, // 30,
  ES_INDEX : process.env.ES_INDEX, //'qiosk-test'
};