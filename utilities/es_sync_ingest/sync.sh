#!/bin/bash
#An example bash script to run syncer.js. Please make a copy of it in another folder before setting it up as a cron job
export WORK_DIR='/home/ubuntu/cgspl.qiosk.search.version.1/utilities/es_sync_ingest' #location of 'es_sync_ingest' folder
export BULK_LIMIT=10; #how many items to index at one go
export ES_SERVER='http://localhost:9200'; #elasticsearch server url
export API_SERVER='http://ec2-52-66-116-94.ap-south-1.compute.amazonaws.com:8003' #point to backend api server
LOGDIR='/home/ubuntu/logs/qiosk_sync_ingest' #logs location
cd $WORK_DIR
node syncer.js >> $LOGDIR/out.log 2>>$LOGDIR/err.log