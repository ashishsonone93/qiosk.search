Usage:
  Edit syncer.js and set ES_SERVER(elasticsearch server url) & API_SERVER(backend server url)
  node syncer.js

Description:
  Used to sync/ingest data from backend mongodb into ES
  
Note:
  It will be run as a cron job (say every minute or every 5 minutes).
  
What it does: For each entity type(Article, Collection, Tag, User)
- Fetch the stored updatedAt value from ES for this entity type
- Fetches 100(configurable number) new data items(using updatedAt & current time values) from backend api server
- Converts some of the fields (like boolean into long) so that it can be used for ES scoring
- Adds additional fields like '#likes', '#shares' for ease of scoring
- Then ingests this data into ES using the Bulk api
- Stores the latest updatedAt among the fetched items in ES itself so that it can be used in the next run.
- Repeat fetchAndIngest for few more rounds if fetched item count was 100 (i.e there could be more data waiting to be ingested).
- If items count was less than 100, we are done.