function bulkInsert(client, sIndex, sType, items){
  var bulkBody = genBulkBody(sIndex, sType, items);
  var promise = client.bulk({
    body : bulkBody
  });

  promise = promise.then(function(r){
    if(r){
      delete r.items;
    }
    console.log("bulkInsert result %j", r);
    return true;
  });
  return promise;  
}

function genBulkBody(sIndex, sType, items){
  var bulkBody = [];
  for(var i in items){
    //if item or its _id is null, ignore
    if(!items[i] || !items[i]._id){
      continue;
    }
    
    var item = items[i];
    bulkBody = bulkBody.concat(toBulk(sIndex, sType, item));
  }
  //console.log("genBulkBody %j", bulkBody);
  return bulkBody;
}

function toBulk (sIndex, sType, item) {
  body = [];
  body.push({
    update: {
      _index: sIndex,
      _type: sType,
      _id: item._id.toString()
    }
  });
  body.push({
    doc: toIndex(item, sType),
    doc_as_upsert: true
  });
  
  return body;
}

function toIndex (item, type) {
  //rename _id to id
  item.id = item._id;
  delete item._id;
  
  //convert boolean to long (so can use in lucene expression during scoring)
  if(type == 'user'){
    item.isInfluencer = item.isInfluencer ? 1 : 0;
    item['#followers'] = item['noOfFollowers'] || 0;
  }
  else if(type == 'tag'){
    item['#articles'] = item['articleCount'] || 0;
  }
  else if(type == 'article'){
    item['#likes'] = (item.likes && item.likes.count) || 0;
    item['#notes'] = (item.notes && item.notes.count) || 0;
    item['#shares'] = (item.shares && item.shares.count) || 0;
  }
  else if(type == 'collection'){
    item['#likes'] = (item.likes && item.likes.count) || 0;
    item['#notes'] = (item.notes && item.notes.count) || 0;
    item['#shares'] = (item.shares && item.shares.count) || 0;
  }
  return item
}

module.exports = {
  bulkInsert : bulkInsert
};