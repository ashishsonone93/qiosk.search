var elasticsearch = require('elasticsearch');
var rp = require('request-promise');
var RSVP = require('rsvp');

var bulkUtils = require('./bulk_utils');
var config = require('./config');

var ES_SERVER = config.ES_SERVER;
var API_SERVER = config.API_SERVER;
var BULK_LIMIT = config.BULK_LIMIT; //how many items to fetch and ingest at a time in bulk
var ES_INDEX = config.ES_INDEX;

const client = new elasticsearch.Client({
  host: ES_SERVER,
  //host : 'ec2-52-66-108-248.ap-south-1.compute.amazonaws.com:9200',
  //log: 'debug'
});

var INDEX = ES_INDEX;

var esToApiMap = {
  'user' : 'users',
  'tag' : 'tags',
  'article' : 'articles',
  'collection' : 'collections'
};

function reverse(tsString, milliseconds){
  var d = new Date(tsString);
  var epoch = d.getTime();
  epoch = epoch - milliseconds;
  d = new Date(epoch);
  return d.toISOString();
}

//get last synced updatedAt timestamp for this type(stored in ES type='sync' in with id=<sType>)
function getStoredUpdatedAt(sIndex, sType){
  var promise = client.get({
    index: sIndex,
    type: 'sync',
    id: sType
  });
  
  //process the updatedAt ts if found
  promise = promise.then(function(result){
    console.log('[%s] get sync %j', sType, result);
    var qs = {};
    if(result && result._source && result._source.updatedAt){
      return result._source.updatedAt;
    }
    return null;
  }, function(err){
    console.log("[%s] get sync err %j", sType, err);
    if(err && err.status == 404){
      return null;
    }
    else{
      throw err;
    }
  });
  
  return promise;
}

function putStoredUpdatedAt(sIndex, sType, updatedAt){
  return client.update({
    index : sIndex,
    type : 'sync',
    id : sType,
    body: {
      doc : {
        updatedAt : updatedAt
      },
      doc_as_upsert: true
    }
  });
}

/*returns a non null
{
  count : #items fetched and indexed
  lastUpdatedAt : updatedAt value of last item
}
*/
function fetchAndIngest(sIndex, sType, startTime, endTime, limit){
  var qs = {};
  if(startTime){
    qs.startTime = startTime;
  }
  qs.endTime = endTime;
  qs.limit = limit;
  qs.source = esToApiMap[sType];

  console.log("[%s] fetchAndIngest %j", sType, qs);
  var promise = rp({
    url: API_SERVER + '/elasticsearch/getInfoByUpdatedAt/',
    qs: qs, //Query string data
    method: 'GET',
    headers: {
      'apikey': 'baby@qiosk',
      'Content-Type' : 'application/json'  
    },
    json: true
  });
  
  //if nothing indexed, then return startTime as lastUpdatedAt
  //this will happen only on 1st round, because in subsequent 
  //rounds we fetch items >= lastUpdatedAt value of last round, //so fetch will bring atleast 1 item 
  var result = {
    count : 0,
    lastUpdatedAt : startTime
  };
  
  //index the items using bulk api
  promise = promise.then(function(items){
    console.log("[%s] items %j", sType, items.map(function(e){return e._id}));
    console.log("[%s] indexing %j items", sType, items.length);
    if(items && items.length > 0){
      result.count = items.length;
      result.lastUpdatedAt = items[items.length-1].updatedAt;
      return bulkUtils.bulkInsert(client, sIndex, sType, items);
    }
    return false;
  });
  
  promise = promise.then(function(success){
    console.log("[%s] items %j", sType, success);
    return result;
  });
  
  return promise;
}

function sync(sIndex, sType){
  var storedUpdatedAt = null;//updatedAt variable fetched from ES index 'sync' for this <type>
  
  var endTime = new Date(); //will only consider data with updatedAt < endTime
  console.log('[%s] current time is %j', sType, endTime.toISOString());  

  endTime = reverse(endTime, 30000);
  
  console.log('[%s] end time is %j', sType, endTime);
  var promise = getStoredUpdatedAt(sIndex, sType);
  
  //1st round of fetchAndIndex
  promise = promise.then(function(ts){
    storedUpdatedAt = ts;
    console.log('[%s] get storedUpdatedAt %j', sType, storedUpdatedAt);

    return fetchAndIngest(sIndex, sType, storedUpdatedAt, endTime, BULK_LIMIT);
  });
  
  //2nd round of fetchAndIndex
  promise = promise.then(function(result){
    console.log('[%s] 1st round result %j', sType, result);

    if(result.count < BULK_LIMIT){
      result.stale = true;
      return result;
    }
    
    return fetchAndIngest(sIndex, sType, result.lastUpdatedAt, endTime, BULK_LIMIT);
  });
  
  //store the 'updatedAt' of last item on success
  promise = promise.then(function(result){
    console.log("[%s] FINAL 2nd round result %j", sType, result);
    
    var newStoredUpdatedAt = null;
    
    if(!result.lastUpdatedAt){
      console.log("[%s] FINAL null lastUpdatedAt", sType);
      //will happen if storedUpdatedAt was null to begin with
      //and no items were fetched at all
    }
    else if(result.count < BULK_LIMIT){
      //all items with value lastUpdatedAt has been fetched and indexed, so can increment by 1 millisecond
      
      //Next time will fetch items >= (lastUpdatedAt + 1ms)
      newStoredUpdatedAt = reverse(result.lastUpdatedAt, -1);
      
      console.log("[%s] FINAL incrementing newStoredUpdatedAt %j", sType, newStoredUpdatedAt);
    }
    else{
      //some items with value lastUpdatedAt might have been missed because count is same as BULK_LIMIT, so store lastUpatedAt as it is. 
      
      //Next time will fetch items >= lastUpdatedAt
      
      newStoredUpdatedAt = result.lastUpdatedAt;
      console.log("[%s] FINAL keep same newStoredUpdatedAt %j", sType, newStoredUpdatedAt);
    }
    
    if(newStoredUpdatedAt){
      console.log("[%s] FINAL storing newStoredUpdatedAt %j", sType, newStoredUpdatedAt);
      putStoredUpdatedAt(sIndex, sType, newStoredUpdatedAt);
    }
    else{
      console.log("[%s] FINAL nothing to store", sType);
    }
    return true;
  });
  
  promise = promise.then(function(result){
    console.log("[%s] FINAL success", sType);
  });
  
  return promise;
}

function work(){
  var p = RSVP.resolve(true);

  p = p.then(function(){
    return sync(INDEX, 'tag');
  });
  
  p = p.then(function(){
    return sync(INDEX, 'user');
  });
  
  p = p.then(function(){
    return sync(INDEX, 'article');
  });
  
  p = p.then(function(){
    return sync(INDEX, 'collection');
  });
  
  p = p.then(function(result){
    console.log("ALL SUCCESS\n");
  });
  
  p.catch(function(err){
    console.log("SOME ERROR %j\n", err);
  });
};


work();

//setInterval(function(){
//  work();
//}, 60000);
