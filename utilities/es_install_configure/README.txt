The script is to be used to install and configure elasticsearch on a fresh ubuntu EC2 instance.

- - - - - - - - - - - -
Usage(step by step):
  #set env var EC2 instance public dns
  EC2=ec2-52-66-107-182.ap-south-1.compute.amazonaws.com
  #set env var KEY
  KEY=~/Documents/AWS/estest.pem

  #copy the script files(2) to EC2 instance
  scp -r -i $KEY es_install_configure ubuntu@$EC2:~

  #Now ssh to EC2 instance
  ssh -i $KEY ubuntu@$EC2

  #Run the script on the EC2 instance
  cd es_install_configure
  chmod 755 es_install.sh
  sudo ./es_install.sh <hostname> <nodename>
  
  #logout from EC2 instance
  logout
  
  #check if up and running by accessing(from your local machine)
  curl $EC2:9200/_cat/health
  curl $EC2:9200/_cat/nodes
  
- - - - - - - - - - - -
What it does:
  installs elasticsearch along with its dependencies(oracle jdk)
  configure the clustername & nodename for this ES node
  enables outside accesss to ES node (from where EC2 security group rules allow)
  registers it as a service to be run on boot