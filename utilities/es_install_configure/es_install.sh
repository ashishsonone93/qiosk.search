#!/bin/bash
if [ "$#" -ne 2 ]; then
    echo "Usage : ./es_install.sh <clustername> <nodename>"
    exit 1
fi

echo "clustername : $1"
echo "nodename : $2"

##install java
sudo add-apt-repository -y ppa:webupd8team/java
sudo apt-get update
echo "oracle-java8-installer shared/accepted-oracle-license-v1-1 select true" | sudo debconf-set-selections
sudo apt-get install -y oracle-java8-installer
java -version


##install elastisearch
mkdir Downloads
cd Downloads
wget https://download.elastic.co/elasticsearch/release/org/elasticsearch/distribution/deb/elasticsearch/2.3.5/elasticsearch-2.3.5.deb
sudo dpkg -i elasticsearch-2.3.5.deb
cd ..

##configure elasticsearch to start on boot
sudo update-rc.d elasticsearch defaults 95 10
sudo /etc/init.d/elasticsearch start


##configure elasticsearch.yml using es_config.py script
sudo python es_config.py /etc/elasticsearch/elasticsearch.yml $1 $2
sudo service elasticsearch restart