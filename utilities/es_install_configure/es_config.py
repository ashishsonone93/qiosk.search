import sys
if len(sys.argv) < 4:
  print("usage : python es_config.py <yml_file> <cluster name> <node name>")
  exit(0)
fname = sys.argv[1]
cluster = sys.argv[2]
node = sys.argv[3]

f = open(fname)
lines = f.readlines()
f.close()

f = open(fname, 'w')

#write the lines(modifying when required)
for line in lines:
  if "node.name" in line:
    line = "node.name: " + node + "\n"
  if "cluster.name" in line:
    line = "cluster.name: " + cluster + "\n"
  if "bootstrap.mlockall" in line:
    line = "bootstrap.mlockall: true" + "\n"
  if "network.host" in line:
    line = "network.host: 0.0.0.0" + "\n"
  f.write(line)

#enable indexed scripts
#line = "\nscript.indexed: true\n"
#f.write(line)

f.close()