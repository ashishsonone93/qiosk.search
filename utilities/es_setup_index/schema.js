var article = {
  id : {
    type : "string",
    index : "no"
  },
  title: {//<search>
    type: "string",
    index : "no",
    fields : {
      article_english : {
        type : "string",
        analyzer : "my_light_english",
        search_analyzer : "my_light_english"
      },
      article_autocomplete : {
        type : "string",
        analyzer : "std_autocomplete",
        search_analyzer : "standard"
      }
    }
  },
  content: {//<search>
    type: "string",
    index : "no",
    fields : {
      article_english : {
        type : "string",
        analyzer : "my_light_english",
        search_analyzer : "my_light_english"
      },
      article_autocomplete : {
        type : "string",
        analyzer : "std_autocomplete",
        search_analyzer : "standard"
      }
    }
  },
  "#likes" : {//<search>
    type : "long"
  },
  "#notes" : {//<search>
    type : "long"
  },
  "#shares" : {//<search>
    type : "long"
  },
  isDeleted : {
    type : "boolean"
  }
};

var collection = {
  id : {
    type : "string",
    index : "no"
  },
  name: {//<search>
    type: "string",
    index : "no",
    fields : {
      collection_english : {
        type : "string",
        analyzer : "my_light_english",
        search_analyzer : "my_light_english"
      },
      collection_autocomplete : {
        type : "string",
        analyzer : "std_autocomplete",
        search_analyzer : "standard"
      }
    }
  },
  description: {//<search>
    type: "string",
    index : "no",
    fields : {
      collection_english : {
        type : "string",
        analyzer : "my_light_english",
        search_analyzer : "my_light_english"
      },
      collection_autocomplete : {
        type : "string",
        analyzer : "std_autocomplete",
        search_analyzer : "standard"
      }
    }
  },
  "#likes" : {//<search>
    type : "long",
  },
  "#notes" : {//<search>
    type : "long",
  },
  "#shares" : {//<search>
    type : "long",
  },
  isDeleted : {
    type : "boolean"
  }
};

var tag = {
  id : {
    type : "string",
    index : "no"
  },
  tagName: {//<search>
    type: "string",
    index : "no",
    fields : {
      tag_english : {
        type: "string",
        analyzer : "my_light_english",
        search_analyzer : "my_light_english"
      },
      tag_autocomplete : {
        type : "string",
        analyzer : "std_autocomplete",
        search_analyzer : "standard"
      }
    }
  },
  tagDescription: {//<search>
    type: "string",
    index : "no",
    fields : {
      tag_english : {
        type: "string",
        analyzer : "my_light_english",
        search_analyzer : "my_light_english"
      },
      tag_autocomplete : {
        type : "string",
        analyzer : "std_autocomplete",
        search_analyzer : "standard"
      }
    }
  },
  "#articles" : {//<search>
    type : "long",
  },
  isDeleted : {
    type : "boolean"
  }
};

var user = {
  id : {
    type : "string",
    index : "no"
  },
  name: {//<search>
    type: "string",
    index : "no",
    fields : {
      user_raw : {
        type: "string",
        analyzer : "raw",
        search_analyzer : "raw"
      },
      user_autocomplete : {
        type : "string",
        analyzer : "raw_autocomplete",
        search_analyzer : "raw"
      }
    }
  },
  "#followers" : {//<search>
    type : "long",
  },
  isInfluencer: {//<search>
    type: "long",
  },
  description: {//<search>
    type : "string",
    index : "no",
    fields : {
      user_english : {
        type: "string",
        analyzer : "my_light_english",
        search_analyzer : "my_light_english"
      },
      user_autocomplete : {
        type : "string",
        analyzer : "std_autocomplete",
        search_analyzer : "standard"
      }
    }
  },
  tagLine: {//<search>
    type : "string",
    index : "no",
    fields : {
      user_english : {
        type: "string",
        analyzer : "my_light_english",
        search_analyzer : "my_light_english"
      },
      user_autocomplete : {
        type : "string",
        analyzer : "std_autocomplete",
        search_analyzer : "standard"
      }
    }
  },
  isDeleted : {
    type : "boolean"
  }
};

module.exports = {
  article : article,
  collection : collection,
  tag : tag,
  user : user,
};