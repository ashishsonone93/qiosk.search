var elasticsearch = require('elasticsearch');
var RSVP = require('rsvp');

var schema = require('./schema');
var settings = require('./settings');
var scripts = require('./scripts');

if(process.argv.length < 4){
  console.log("usage : node init_setup.js <ES_HOST> <ES_INDEX_NAME>");
  console.log("e.g node init_setup.js http://localhost:9200 qiosk-test");
  process.exit(0);
}

var ES_HOST = process.argv[2]; //ES host name
var INDEX = process.argv[3]; //ES index name

console.log(ES_HOST + "/" + INDEX);

const client = new elasticsearch.Client({
  host: ES_HOST,
  //log: 'debug',
  apiVersion : '2.3'
});

//waits for 1s (to ensure the index has been properly created)
function wait(r){
  console.log("wait %j", r);
  var promise = new RSVP.Promise(function(resolve, reject){
    setTimeout(function(){
      resolve(true);
    }, 1000);
  });
  return promise;
}

//destroy the index (alongwith data)
function deleteIndex(r){
  console.log("delete %j", r);
  var promise = client.indices.delete({ index: INDEX });
  promise = promise.catch(function(err){
    if(err.statusCode == 404){
      return 'NO_INDEX_TO_DELETE';
      throw err;
    }
  });
  return promise;
}

//create the index
function createIndex(r){
  console.log("create %j", r);
  var promise = client.indices.create({ index: INDEX });
  return promise;
}

//close index to update non-dynamic settings
function closeIndex(r){
  console.log("close %j", r);
  var promise = client.indices.close({ index : INDEX });
  return promise;
}

//open index after changing settings
function openIndex(r){
  console.log("open %j", r);
  var promise = client.indices.open({ index : INDEX });
  return promise;
}

//create filters and analyzers
function putSettings(r){
  console.log("settings %j", r);
   
  var promise = client.indices.putSettings({
    "index" : INDEX,
    "body" : {
      "analysis" : {
        "analyzer" : settings.analyzer,
        "filter" : settings.filter
      }
    }
  });
  
  return promise;
}

function putArticleMapping(r){
  console.log("article mapping %j", r);
  
  var promise = client.indices.putMapping({
    index : INDEX,
    type : 'article',
    body : {
      dynamic : false,
      properties : schema.article
    }
  });
  
  return promise;
}

function putCollectionMapping(r){
  console.log("collection mapping %j", r);
  
  var promise = client.indices.putMapping({
    index : INDEX,
    type : 'collection',
    body : {
      dynamic : false,
      properties : schema.collection
    }
  });
  
  return promise;
}

function putTagMapping(r){
  console.log("tag mapping %j", r);
  
  var promise = client.indices.putMapping({
    index : INDEX,
    type : 'tag',
    body : {
      dynamic : false,
      properties : schema.tag
    }
  });
  
  return promise;
}

function putUserMapping(r){
  console.log("user mapping %j", r);

  var promise = client.indices.putMapping({
    index : INDEX,
    type : 'user',
    body : {
      dynamic : false,
      properties : schema.user
    }
  });
  
  return promise;
}

function putRankProfileScript(r){
  console.log("profile script %j", r);
  
  var promise = client.putScript({
    id : 'rank_profile',
    lang : 'groovy',
    body : {
      script : scripts.rank_profile
    }
  });
  
  return promise;
}

function putRankArticleScript(r){
  console.log("article script %j", r);
  
  var promise = client.putScript({
    id : 'rank_article',
    lang : 'groovy',
    body : {
      script : scripts.rank_article
    }
  });
  
  return promise;
}

function deleteCreateIndex(){
  return RSVP.resolve(true)
  .then(deleteIndex)
  .then(createIndex)
  .then(wait);
}

function closeSettingsOpen(){
  return RSVP.resolve(true)
  .then(closeIndex)
  .then(putSettings)
  .then(openIndex)
}

var p = RSVP.resolve(true)
  .then(deleteCreateIndex)
  .then(closeSettingsOpen)
  .then(putArticleMapping)
  .then(putCollectionMapping)
  .then(putTagMapping)
  .then(putUserMapping)
  //.then(putRankProfileScript)
  //.then(putRankArticleScript)

p.then(function(r){
  console.log("SUCCESS %j", r);
});

p.catch(function(err){
  console.log("ERROR %j", err);
});