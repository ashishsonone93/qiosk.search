This script is to setup the elasticsearch index for use.

- - - - - - - - - - - -
Precisely, it does following:
1) deletes the index (if already exists) - as this is for one time setup only - be careful not to run on existing elasticsearch index
2) creates the index
3) creates analyzers and filters to be used for data
4) creates mapping(schema) for each object type - what fields will be indexed and how (specifying field data type, analyzer used for indexing & searching)
5) creates and stores scripts to be used for scoring search results

- - - - - - - - - - - -
Usage:
  Ensure that ES node is accessible from your machine(according to EC2 security group rules)
  install the dependencies 
    in root folder of this repo, run 'npm install'
  go to 'es_setup_index' folder
  Run
    node init_setup.js <ES_HOST> <INDEX_NAME>
    e.g node init_setup.js http://localhost:9200 qiosk-test
  And voila ! you're done
  
- - - - - - - - - - - -
Other details:
'schema.js' : contains the field mapping for each object type
'settings.js' : contains the analyer and filter definitions
'scripts.js' : contains script definitions