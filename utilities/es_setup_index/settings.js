var analyzer = {
  /*
  For reference : default standard analyzer definition
  "standard" : {
    "tokenizer": "standard",
    "filter":  [ "lowercase", "stop" ]
  }
  */
  
  //standard analyzer with autocomplete filter
  "std_autocomplete": {
    "char_filter":  ["html_strip"],
    "tokenizer":  "standard",
    "filter": [
      "lowercase",
      "stop",
      "autocomplete_filter"
    ]
  },
  
  //standard analyzer with possessive eng stemmer + light eng stemmer
  "my_light_english" : {
    "char_filter":  ["html_strip"],
    "tokenizer":  "standard",
    "filter": [
      "lowercase",
      "possessive_english_stemmer",
      "stop",
      "light_english_stemmer"
    ]
  },
  
  //just lower with autocomplete - for name field
  "raw_autocomplete" : {
    "char_filter":  ["html_strip"],
    "tokenizer":  "standard",
    "filter": [
      "lowercase",
      "autocomplete_filter"
    ]
  },
  
  //just lower with autocomplete - for name field
  "raw" : {
    "char_filter":  ["html_strip"],
    "tokenizer":  "standard",
    "filter": [
      "lowercase"
    ]
  }
};

var filter = {
  "autocomplete_filter" : {
    "type": "edge_ngram", 
    "min_gram": 1, 
    "max_gram": 20
  },
  "light_english_stemmer": {
    "type": "stemmer",
    "language": "light_english"
  },
  "possessive_english_stemmer" : {
    "type" : "stemmer",
    "language" : "possessive_english"
  }
};

module.exports = {
  analyzer : analyzer,
  filter : filter,
};