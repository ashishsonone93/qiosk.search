var rank_profile = "log10(1 + doc['followers'])/8";
var rank_article = "log10(1 + doc['likes'] + 3 * doc['comments'] + 3 * doc['shares'])";

module.exports = {
  rank_profile : rank_profile,
  rank_article : rank_article
};