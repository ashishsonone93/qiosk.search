The module 'qiosk_search' implements the search part of Qiosk project

This consists of four parts(folders):
1) api
  This is the search api server code
  For more details, refer to api/README.txt
  
2) utilities:
  These are the utitities for the following tasks:
  a) es_install_configure : script to install and configure ES on ubuntu EC2 instance.
  b) es_setup_index : nodejs script to set up ES index, create analyzers/filters, and schema(mapping) for each type of object.
  c) es_sync_ingest : nodejs script to sync(ingest) updated items fetched from backend api into the ES node. This needs to be run as a cron job every minute or so.
  
  To know more about these utilities and how to use them, refer to their README.txt in corresponding folders
 
3) unit_tests:
  These contain some tests written using mocha and chai to test certain parts of the search api : particularly to see if the ES is queried with proper params.
  Will keep adding more tests to improve coverage
  
4) testing contains the code to simulate and do integration testing : running backend(simulated), generating the json items from scraped data, inserting the data into backend(via script).
  So that we can:
  - check if the syncer.js (utilities/es_sync_ingest) cron routine is doing its ingestion job correctly
  - try out search api (atleast on this generated data)
  
  For more details : refere to testing/README.txt
  
