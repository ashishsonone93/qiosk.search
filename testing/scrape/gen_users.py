import json
import datetime, pytz
import sys
from random import randint

fname = sys.argv[1]
print("scraping source " + fname)
f = open(fname)
lines = f.readlines()

profiles = []

for i in range(len(lines)):
  name = lines[i]
  tokens = name.split()
  name = " ".join(tokens)
  
  isInfluencer = False
  if randint(0, 100) < 10:
    isInfluencer = True
    
  followers = randint(0,30)
  
  if(isInfluencer):
    followers = randint(30, 200)
  
  profile = {}
  profile['_id'] = "user-" + str(i)
  profile['name'] = name
  profile['emailId'] = profile['_id'] + "@gmail.com"

  if randint(0, 100) < 20:
    profile['facebookProfile'] = profile['_id']
  if randint(0, 100) < 20:
    profile['twitterProfile'] = profile['_id']
  if randint(0, 100) < 20:
    profile['linkedinProfile'] = profile['_id']
  if randint(0, 100) < 20:
    profile['googleProfile'] = profile['_id']

  profile['username'] = profile['_id'];
  profile['password'] = 'pass-' + profile['_id'];

  if randint(0, 100) < 20:
    profile['profilePicture'] = 'pic-' + profile['_id'] + '.jpg'
  if randint(0, 100) < 20:
    profile['profilePictureThumbnail'] = 'thumb-' + profile['_id'] + '.jpg'
  if randint(0, 100) < 20:
    profile['interests'] = ['x', 'y', 'z']
  if randint(0, 100) < 10:
    profile['userType'] = 'normalUser'
  else:
    profile['userType'] = 'adminUser'

  profile['isInfluencer'] = isInfluencer

  if randint(0, 100) < 10:
    location = {}
    location['latLong'] = str(randint(10, 90)) + ',' + str(randint(10, 90))
    location['city'] = 'city-' + str(randint(10, 1000))
    location['state'] = 'state-' + str(randint(10, 200))
    location['country'] = 'country-' + str(randint(10, 50))
    location['pinCode'] = randint(5000000, 9000000)
    profile['userLocation'] = location

  profile['tagLine'] = 'when life gives you lemons ...'
  profile['description'] = 'hello, I am ' + name + ' & I will be back'
  profile['lastLogin'] = datetime.datetime.now(pytz.timezone('Asia/Kolkata')).isoformat()

  profiles.append(profile)
  
oname = fname.replace('txt', 'json')
print("storing json in " + oname)
print("storing #" + str(len(profiles)))
out = open(oname, 'w')
json.dump(profiles, out)