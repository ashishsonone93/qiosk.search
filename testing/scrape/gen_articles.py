import re
import json
import sys
import datetime, pytz
from random import randint

SEED = 0 #_id = SEED + _id

def getISOTime():
  return datetime.datetime.now(pytz.timezone('Asia/Kolkata')).isoformat()

fname = sys.argv[1]
print("scraping source " + fname)
f = open(fname)
lines = f.readlines()

articles = []

for index in range(len(lines)/3):
  l1 = lines[index*3]
  l2 = lines[index*3+1]
  l3 = lines[index*3+2]
  
  _id = int(l1.strip().strip('.'))

  l2_tokens = re.split('[\(\)]', l2.strip())
  
  title = l2_tokens[0]
  
  source = "None " if len(l2_tokens) < 2 else l2_tokens[1]
  m = re.search(r"(\d+) points", l3)
  numlikes = "0" if m == None else m.group(1)
  numlikes = int(numlikes)
  m = re.search(r"(\d+) comments", l3)
  numcomments = "0" if m == None else m.group(1)
  numcomments = int(numcomments)
  m = re.search(r"(\d+) hours", l3)
  numshares = "0" if m == None else m.group(1)
  numshares = int(numshares)

  article = {}
  article['_id'] = 'article-' + str(SEED + _id)
  article['title'] = title
  article['author'] = 'user-' + str(randint(10, 1000))
  article['content'] = '<html><body><div>' + title + '</div><br><div>' + title + '</div></body></html>'
  article['metadata'] = 'metadata-' + article['_id']
  article['shortDescription'] = 'description-' + article['_id']
  article['imageArray'] = ['a.jpg', 'b.jpg', 'c.jpg']
  article['coverImage'] = 'image-' + article['_id'] + '.jpg'
  article['tags'] = ['a', 'b', 'c']

  if randint(0, 100) < 80:
    article['articleType'] = 'original'
  else:
    article['articleType'] = 'referenced'

  article['url'] = 'url-' + article['_id']
  article['originalUrl'] = 'original-' + article['url']

  #likes
  if randint(0, 100) < 50:
    likes = {};
    likes['count'] = numlikes
    details = []
    for i in range((9+numlikes)/10):
      like = {}
      like['userId'] = 'user-' + str(randint(10, 1000))
      like['createdAt'] = getISOTime()
      details.append(like)
    likes['details'] = details
    article['likes'] = likes

  #notes
  if randint(0, 100) < 50:
    notes = {};
    notes['count'] = numcomments
    details = []
    for i in range((9+numcomments)/10):
      note = {}
      note['userId'] = 'user-' + str(randint(10, 1000))
      note['createdAt'] = getISOTime()
      note['updatedAt'] = getISOTime()
      note['note'] = 'This is a random comment'
      if randint(0, 100) < 10:
        reply = []
        numreply = randint(1, 6)
        for i in range(numreply):
          r = {}
          r['userId'] = 'user-' + str(randint(10, 1000))
          r['content'] = 'reply to a reply'
          r['repliedAt'] = getISOTime()
          reply.append(r)
        note['reply'] = reply
      details.append(note)
    notes['details'] = details
    article['notes'] = notes

  #shares
  if randint(0, 100) < 50:
    shares = {};
    shares['count'] = numshares
    details = []
    for i in range((9+numshares)/10):
      share = {}
      share['sharedBy'] = 'user-' + str(randint(10, 1000))
      share['sharedUrl'] = 'url-' + str(randint(100, 10000))
      share['sharedAt'] = getISOTime()
      details.append(share)
    shares['details'] = details
    article['shares'] = shares

  articles.append(article)
  
oname = fname.replace('txt', 'json')
print("storing json in " + oname)
print("storing #" + str(len(articles)))
out = open(oname, 'w')
json.dump(articles, out)