import json
import sys
from random import randint
fname = sys.argv[1]
print("scraping source " + fname)
f = open(fname)
lines = f.readlines()

topics = []

for i in range(len(lines)):
  name = lines[i]
  name = name.strip()
  topic = {}
  topic['_id'] = "tag-" + str(i)
  topic['tagName'] = name
  topic['tagDescription'] = 'this is tag #' + str(i) + ' with name ' + name
  topic['createdBy'] = 'user-' + str(randint(10,1000))
  topics.append(topic)
  
oname = fname.replace('txt', 'json')
print("storing json in " + oname)
print("storing #" + str(len(topics)))
out = open(oname, 'w')
json.dump(topics, out)