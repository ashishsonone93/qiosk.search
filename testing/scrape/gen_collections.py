import re
import json
import sys
import datetime, pytz
from random import randint

SEED = 500 #_id = SEED + _id

def getISOTime():
  return datetime.datetime.now(pytz.timezone('Asia/Kolkata')).isoformat()

fname = sys.argv[1]
print("scraping source " + fname)
f = open(fname)
lines = f.readlines()

collections = []

for index in range(len(lines)):
  l1 = lines[index]
  tokens = l1.split('\t') #split on tab character
  if len(tokens) < 2:
    print("skip:" + l1)
    continue
  name = tokens[0].strip()
  description = tokens[1].strip()
  
  _id = 'collection-' + str(index)

  numlikes = randint(0, 40)
  if randint(0, 100) < 20:
    numlikes = randint(40, 400)

  numcomments = randint(0, 30)
  if randint(0, 100) < 20:
    numcomments = randint(30, 300)
  
  numshares = randint(0, 20)
  if randint(0, 100) < 20:
    numshares = randint(20, 200)
    
  collection = {}
  collection['_id'] = _id
  collection['name'] = name
  collection['description'] = description

  collection['userId'] = 'user-' + str(randint(10, 1000))

  numarticles = randint(0, 30)
  articles = []
  for i in range(numarticles):
    articles.append('article-' + str(randint(100, 10000)))
  collection['articles'] = articles

  collection['url'] = 'url-collection-' + str(randint(100, 10000))
  collection['collectionImage'] = 'image-' + str(randint(10, 1000)) + '.jpg'

  #likes
  if randint(0, 100) < 50:
    likes = {};
    likes['count'] = numlikes
    details = []
    for i in range((9+numlikes)/10):
      like = {}
      like['userId'] = 'user-' + str(randint(10, 1000))
      like['createdAt'] = getISOTime()
      details.append(like)
    likes['details'] = details
    collection['likes'] = likes

  #notes
  if randint(0, 100) < 50:
    notes = {};
    notes['count'] = numcomments
    details = []
    for i in range((9+numcomments)/10):
      note = {}
      note['userId'] = 'user-' + str(randint(10, 1000))
      note['createdAt'] = getISOTime()
      note['updatedAt'] = getISOTime()
      note['note'] = 'This is a random comment'
      if randint(0, 100) < 10:
        reply = []
        numreply = randint(1, 6)
        for i in range(numreply):
          r = {}
          r['userId'] = 'user-' + str(randint(10, 1000))
          r['content'] = 'reply to a reply'
          r['repliedAt'] = getISOTime()
          reply.append(r)
        note['reply'] = reply
      details.append(note)
    notes['details'] = details
    collection['notes'] = notes

  #shares
  if randint(0, 100) < 50:
    shares = {};
    shares['count'] = numshares
    details = []
    for i in range((9+numshares)/10):
      share = {}
      share['sharedBy'] = 'user-' + str(randint(10, 1000))
      share['sharedUrl'] = 'url-' + str(randint(100, 10000))
      share['sharedAt'] = getISOTime()
      details.append(share)
    shares['details'] = details
    collection['shares'] = shares

  collections.append(collection)
  
oname = fname.replace('txt', 'json')
print("storing json in " + oname)
print("storing #" + str(len(collections)))
out = open(oname, 'w')
json.dump(collections, out)