var fs = require('fs');
var rp = require('request-promise');
var _ = require('underscore');

var type = process.argv[2];
var dataFile = process.argv[3];
var count = process.argv[4];

var SERVER = process.env.SERVER || "http://localhost:8003";

if(!(type && dataFile && count)){
  console.log("usage : node populator.js <type> <data file> <count>");
  console.log("node populator.js tags ../../test_data_quora_topics/data.json 5");
  process.exit(0);
}

function readData(){
  fs.readFile(dataFile, 'utf8', function (err,data) {
    if (err) {
      return console.log(err);
    }
    var items = JSON.parse(data);
    var items = _.sample(items, count);
    //console.log('%j', items[0]);
    for(var i in items){
      var item = items[i];
      save(item);
    }
  });
}

function save(item){
  var id = item._id;
  var reqUrl = SERVER + "/" + type + "/" + id;
  //console.log('track`' + reqUrl + '`track');
  delete item._id;

  var p = rp({
    url: reqUrl,
    body: item,
    method: 'PUT',
    json: true
  });

  p = p.then(function(result){
    console.log("success %j", id);
  });
  return p;
}

console.log("SERVER %j", SERVER);
console.log("type %j", type);
console.log("dataFile %j", dataFile);

readData();