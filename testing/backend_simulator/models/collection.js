var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CollectionSchema = Schema({
  _id : String,
  name: String,
  description : String,

  userId: String, //id
  articles: [String], //id
  url: String,
  collectionImage:String,
  likes: {
    count: {
      type: Number,
      default: 0
    },
    details: [{
      userId: String, //id
      createdAt: Date
    }]
  },
  notes: {
    count: {
      type: Number,
      default: 0
    },
    details: [{
      userId: String, //id
      createdAt: Date,
      updatedAt: Date,
      note: String,
      reply: [{
        userId: String, //id
        content: String,
        repliedAt: Date
      }]
    }]
  },
  shares: {
    count: {
      type: Number,
      default: 0
    },
    details: [{
      sharedBy: String, //id
      sharedUrl: String,
      sharedAt: Date
    }]
  },
  isDeleted:{type:Boolean,default:false}
},
{
  timestamps : {} //assigns default createdAt and updatedAt fields
});

module.exports = {
  model : mongoose.model('Collection', CollectionSchema, 'collections'),
};