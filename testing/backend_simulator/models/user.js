var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = Schema({
  _id : String,
  name: String,
  emailId: String,
  facebookProfile: String,
  twitterProfile: String,
  linkedinProfile: String,
  googleProfile: String,
  username: String,
  password: String,
  profilePicture: String,
  profilePictureThumbnail: String,
  interests: [String], //id
  userType: {type: String, enum: ['normalUser', 'adminUser']},
  isInfluencer: Boolean,
  userLocation:{
    latLong:String,
    city:String,
    state:String,
    country:String,
    pinCode:Number
  },
  tagLine: String,
  description: String,
  lastLogin: {type: Date},
  isDeleted:{type:Boolean,default:false}
},
{
  timestamps : {} //assigns default createdAt and updatedAt fields
});

module.exports = {
  model : mongoose.model('User', UserSchema, 'users'),
};