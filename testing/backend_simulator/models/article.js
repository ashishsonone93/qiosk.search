var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ArticleSchema = Schema({
  _id : String,
  title: String,
  author: String, //id
  content: String,
  metadata : String,
  shortDescription : String,
  imageArray : [String],
  coverImage: String,
  tags: [String],//id
  articleType: {type: String, enum: ['original', 'referenced']},
  url: String,
  originalUrl : String,
  likes: {
    count: {
      type: Number,
      default: 0
    },
    details: [{
      userId: String, //id
      createdAt: Date
    }]
  },
  notes: {
    count: {
      type: Number,
      default: 0
    },
    details: [{
      userId: String, //id
      createdAt: Date,
      updatedAt: Date,
      note: String,
      reply: [{
        userId: String, //id
        content: String,
        repliedAt: Date
      }]
    }]
  },
  shares: {
    count: {
      type: Number,
      default: 0
    },
    details: [{
      sharedBy: String, //id
      sharedUrl: String,
      sharedAt: Date
    }]
  },
  isDeleted:{type:Boolean,default:false}
},
{
  timestamps : {} //assigns default createdAt and updatedAt fields
});

module.exports = {
  model : mongoose.model('Article', ArticleSchema, 'articles'),
};