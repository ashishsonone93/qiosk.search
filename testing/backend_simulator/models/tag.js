var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TagSchema = Schema({
  _id : String,
  tagName: String,
  tagDescription: String,
  createdBy: String,
  isDeleted:{type:Boolean,default:false}
},
{
  timestamps : {} //assigns default createdAt and updatedAt fields
});

module.exports = {
  model : mongoose.model('Tag', TagSchema, 'tags'),
};