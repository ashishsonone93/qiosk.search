var mongoose = require('mongoose');
var express = require('express');
var bodyParser = require('body-parser');

var app = express();
app.use(bodyParser.urlencoded({extended : true})); // parse application/x-www-form-urlencoded
app.use(bodyParser.json()); // parse application/json

var UserModel = require('./models/user').model;
var TagModel = require('./models/tag').model;
var ArticleModel = require('./models/article').model;
var CollectionModel = require('./models/collection').model;

var PORT = 8003;

mongoose.connect("mongodb://localhost:27017/qiosk-test", function(err, result){
  console.log("mongo connection result %j %j", err, result);
});

var MODEL_MAP = {
  'user' : UserModel,
  'tag' : TagModel,
  'article' : ArticleModel,
  'collection' : CollectionModel
};

function post(type, req, res){
  var model = MODEL_MAP[type];
  var object = new model(req.body);
  
  var promise = object.save();
  
  promise.then(function(result){
    res.json(result);
  });
  promise.catch(function(err){
    res.status(500);
    res.json(err);
  });
}

function put(type, req, res){
  var model = MODEL_MAP[type];
  
  var id = req.params.id;
  var object = req.body;
  
  var promise = model.findOneAndUpdate(
    {_id : id}, 
    object,
    {
      new : true,
      upsert : true
    }).exec();
  
  promise.then(function(result){
    res.json(result);
  });
  
  promise.catch(function(err){
    res.status(500);
    res.json(err);
  });
}

function get(type, req, res){
  var model = MODEL_MAP[type];
  
  var limit = 100;
  if(req.query.limit){
    limit = req.query.limit;
  }
  
  limit = parseInt(limit);
  
  var findQuery = {};
  var gte = req.query.gte;
  var lt = req.query.lt;

  if(gte || lt){
    findQuery.updatedAt = {};
    if(gte){
      findQuery.updatedAt['$gte'] = gte;
    }
    if(lt){
      findQuery.updatedAt['$lt'] = lt;
    }
  }

  var sort = {
    updatedAt : 1
  };
  
  var promise = model
    .find(findQuery)
    .sort(sort)
    .limit(limit)
    .exec();
  promise = promise.then(function(result){
    res.json(result);
  });
  
  promise.catch(function(err){
    res.status(500);
    res.json(err);
  });
}

function getById(type, req, res){
  var model = MODEL_MAP[type];
  var id = req.params.id;
  
  var promise = model
    .findOne({_id : id})
    .exec();
  
  promise = promise.then(function(object){
    res.json(object);
  });
  
  promise.catch(function(err){
    res.status(500);
    res.json(err);
  });
}

function genRoutes(apiPath, esTypeName, app){
  app.post(apiPath, function(req, res){
    post(esTypeName, req, res);
  });
  
  app.put(apiPath + '/:id', function(req, res){
    put(esTypeName, req, res);
  });
  
//  app.get(apiPath, function(req, res){  
//    get(esTypeName, req, res);
//  });
  
  app.get(apiPath + '/:id', function(req, res){
    getById(esTypeName, req, res);
  });
}

function getInfoByUpdatedAt(req, res){
  var source = req.query.source;
  if(!source || !req.query.endTime){
    res.status(401);
    return res.json({
      err : 'params required : [endTime, source], optional : [startTime, limit]'
    });
  }
  
  //rename params (startTime=>gte, endTime=>lt)
  req.query.gte = req.query.startTime;
  req.query.lt = req.query.endTime;
  
  type = source.substr(0, source.length-1);//e.g 'articles' => 'article'
  get(type, req, res);
}

genRoutes('/users', 'user', app);
genRoutes('/tags', 'tag', app);
genRoutes('/articles', 'article', app);
genRoutes('/collections', 'collection', app);

app.get('/elasticsearch/getInfoByUpdatedAt', getInfoByUpdatedAt);

app.listen(PORT);
console.log("server listening on localhost:8003");