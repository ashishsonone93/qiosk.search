1.	
Our primary goal is to un-fork the Tor Browser (mozilla.org)
269 points by dao- 9 hours ago | hide | 73 comments
2.	
Show HN: Cuckoo Filter Implementation in Go, Better Than Bloom Filters (github.com)
41 points by irfansharif 4 hours ago | hide | 1 comment
3.	
IBM Watson correctly diagnoses a form of leukemia (siliconangle.com)
207 points by adamnemecek 12 hours ago | hide | 59 comments
4.	
In Nuclear Silos, Death Wears a Snuggie (2011) (wired.com)
37 points by wallflower 5 hours ago | hide | 14 comments
5.	
Show HN: Gping.io – Like TinyURL for your car (gping.io)
118 points by dustball 10 hours ago | hide | 52 comments
6.	
Tabby's star is dimming at an incredible rate (sciencealert.com)
290 points by ChuckMcM 17 hours ago | hide | 131 comments
7.	
The 39th Root of 92 (bit-player.org)
145 points by bit-player 13 hours ago | hide | 70 comments
8.	
A x64 OS #1: UEFI (kazlauskas.me)
191 points by based2 18 hours ago | hide | 50 comments
9.	
Can you short Uber? (qz.com)
30 points by uptown 6 hours ago | hide | 30 comments
10.	
Love in Translation: Learning about culture, communication, and intimacy (newyorker.com)
19 points by wallflower 5 hours ago | hide | discuss
11.	
Firejail – security sandbox (firejail.wordpress.com)
90 points by simonpure 14 hours ago | hide | 21 comments
12.		
Impraise (YC S14) Is Hiring Ruby Developers in Beautiful Amsterdam (impraise.com)
53 minutes ago | hide
13.	
Binary Ninja – A new kind of reversing platform (binary.ninja)
79 points by Philipp__ 12 hours ago | hide | 28 comments
14.	
Lithium-air batteries: Their time has come (economist.com)
77 points by mdani 13 hours ago | hide | 67 comments
15.	
Bulma – A modern CSS framework based on Flexbox (bulma.io)
72 points by reimertz 10 hours ago | hide | 26 comments
16.	
Show HN: Set of trained deep learning models for computer vision (github.com)
93 points by fchollet 16 hours ago | hide | 11 comments
17.	
The brain's reward circuitry regulates immunity (nature.com)
107 points by danielmorozoff 16 hours ago | hide | 24 comments
18.	
Codemoji (codemoji.org)
38 points by doener 11 hours ago | hide | 6 comments
19.	
DLS, the digital logic simulator game (itch.io)
100 points by jsnell 19 hours ago | hide | 22 comments
20.	
Show HN: Checkup – OSS tool for simple, self-hosted health checks (sourcegraph.com)
47 points by tim_l 11 hours ago | hide | 7 comments
21.	
Eskil – a GUI front end to diff (tcl.tk)
61 points by networked 14 hours ago | hide | 15 comments
22.	
Bodies Left on Mt. Everest (imgur.com)
5 points by yonibot 59 minutes ago | hide | discuss
23.	
Show HN: Make Slack Bots in Java (github.com)
63 points by ramswaroop 14 hours ago | hide | 18 comments
24.	
Superblocks: how Barcelona is taking city streets back from cars (vox.com)
342 points by based2 22 hours ago | hide | 185 comments
25.	
High-Fidelity Quantum Logic Gates (aps.org)
52 points by gk1 16 hours ago | hide | 8 comments
26.	
The Code: A declassified hostage rescue story (2015) (theverge.com)
53 points by wglb 18 hours ago | hide | 7 comments
27.	
How millions of trees brought a broken landscape back to life (theguardian.com)
17 points by mafro 2 hours ago | hide | 6 comments
28.	
Saving social media for posterity (nytimes.com)
36 points by mattdennewitz 17 hours ago | hide | 12 comments
29.	
Bitfinex Interim Update (bitfinex.com)
37 points by Heliosmaster 17 hours ago | hide | 29 comments
30.	
Frequent Password Changes Is a Bad Security Idea (schneier.com)
89 points by alanfranzoni 12 hours ago | hide | 28 comments
31.	
Throughput vs. Latency and Lock-Free vs. Wait-Free (concurrencyfreaks.blogspot.com)
123 points by ingve 21 hours ago | hide | 29 comments
32.	
PlayStation Vita Exploit Reverse Engineering Challenge (yifan.lu)
107 points by gdk 21 hours ago | hide | 9 comments
33.	
Mozilla Awards $585k to Nine Open Source Projects (mozilla.org)
427 points by kibwen 1 day ago | hide | 138 comments
34.	
From Linear Models to Machine Learning (draft) [pdf] (ucdavis.edu)
131 points by sonabinu 21 hours ago | hide | 16 comments
35.	
Minds Turned to Ash: Burnout is more than working too hard (1843magazine.com)
309 points by xhrpost 1 day ago | hide | 84 comments
36.	
Hello from Orkut (orkut.com)
626 points by shardul111 2 days ago | hide | 317 comments
37.	
Implementation of various string similarity and distance algorithms in Java (github.com)
78 points by based2 21 hours ago | hide | 21 comments
38.	
API providing threat analysis of any given IP address (fraudguard.io)
112 points by dontbesalty 18 hours ago | hide | 71 comments
39.	
The Linux kernel hidden inside Windows 10 (github.com)
318 points by eDameXxX 1 day ago | hide | 156 comments
40.	
Desktop privacy & security of web browsers on Linux part 1: concepts and theory (nexlab.net)
71 points by nextime 20 hours ago | hide | 7 comments
41.	
Alonzo Church, 92, Theorist of the Limits of Mathematics(1995) (nytimes.com)
3 points by kercker 8 hours ago | hide | discuss
42.	
Sheriff Raids House to Find Anonymous Blogger Who Called Him Corrupt (theintercept.com)
280 points by guiambros 19 hours ago | hide | 73 comments
43.	
Avalonia Alpha 4 – A cross-platform .NET UI framework (grokys.github.io)
89 points by grokys 23 hours ago | hide | 18 comments
44.	
Introducing Amazon One (amazon.com)
24 points by t23 19 hours ago | hide | 5 comments
45.	
Every Year on August 5th, the Curiosity Rover Sings a Lonely Birthday Song (curiosity.com)
81 points by s_q_b 1 day ago | hide | 35 comments
46.	
Ask HN: How do you get notified about newest research papers in your field?
373 points by warriorkitty 1 day ago | hide | 133 comments
47.	
“EFI? Intel has been trying to shove that down our throats for years.” (2003) (groups.google.com)
45 points by yuhong 14 hours ago | hide | 14 comments
48.	
Praise for Intelligence Can Undermine Children's Motivation (1998) [pdf] (stanford.edu)
240 points by EvgeniyZh 1 day ago | hide | 152 comments
49.	
Stand up against the stand up (techcrunch.com)
3 points by lancefisher 9 hours ago | hide | discuss
50.	
Carnegie Mellon’s Mayhem AI Wins DARPA’s Cyber Grand Challenge (techcrunch.com)
158 points by ebakan 1 day ago | hide | 26 comments
51.	
South Koreans use emoji to express playful sentiments they wouldn’t utter aloud (1843magazine.com)
62 points by r0n0j0y 21 hours ago | hide | 24 comments
52.	
MIT and DARPA Pack Lidar Sensor onto Single Chip (ieee.org)
254 points by Osiris30 1 day ago | hide | 55 comments
53.	
7-Elevens Are Becoming Lifelines for Japan's Elderly (citylab.com)
113 points by wallflower 1 day ago | hide | 42 comments
54.	
A Lighter and Smaller Laptop (kartick-log.blogspot.com)
34 points by kartickv 1 day ago | hide | 70 comments
55.	
What are Bloom filters? (2015) (medium.com)
187 points by trumbitta2 1 day ago | hide | 68 comments
56.	
Guided in-process fuzzing of Chrome components (googleblog.com)
24 points by ivank 1 day ago | hide | 1 comment
57.	
The world's first website went online 25 years ago today (telegraph.co.uk)
28 points by acangiano 13 hours ago | hide | 10 comments
58.	
The women who made communication with outer space possible (pri.org)
32 points by molecule 14 hours ago | hide | discuss
59.	
Latest to Quit Google’s Self-Driving Car Unit: Top Roboticist (nytimes.com)
123 points by Qworg 1 day ago | hide | 88 comments
60.	
A molecule called ‘Sandman’ could help solve the ‘mystery of sleep’ (washingtonpost.com)
127 points by chriskanan 1 day ago | hide | 61 comments
61.	
A molecule called ‘Sandman’ could help solve the ‘mystery of sleep’ (washingtonpost.com)
127 points by chriskanan 1 day ago | hide | 61 comments
62.	
Apple announces bug bounty program (techcrunch.com)
338 points by nos4A2 2 days ago | hide | 90 comments
63.	
Amazon launches Prime Air, its own dedicated cargo planes to speed delivery (techcrunch.com)
269 points by prostoalex 2 days ago | hide | 179 comments
64.	
Notes on concurrency bugs (danluu.com)
95 points by joeyespo 1 day ago | hide | 25 comments
65.	
Test flight held for small jet modeled after Miyazaki anime (mainichi.jp)
238 points by sjreese 1 day ago | hide | 69 comments
66.	
Stack Computers: the new wave (1989) (cmu.edu)
33 points by kercker 21 hours ago | hide | 28 comments
67.	
Let's Encrypt root certificate trusted by Mozilla (mozilla.org)
445 points by _jomo 1 day ago | hide | 155 comments
68.	
What I learned from the Chinese about our Indian online market (reverieinc.com)
90 points by pakhibagai 1 day ago | hide | 64 comments
69.	
The LHC “nightmare scenario” has come true (backreaction.blogspot.com)
288 points by another 20 hours ago | hide | 189 comments
70.	
WeChat’s world (economist.com)
13 points by g4k 18 hours ago | hide | 5 comments
71.	
Show HN: Book about writing web apps in Go without a framework (github.com)
150 points by thewhitetulip 1 day ago | hide | 55 comments
72.	
Making Rust Fly with MIR [video] (mozilla.org)
97 points by bluejekyll 1 day ago | hide | 10 comments
73.	
Xen exploitation part 3: XSA-182, Qubes escape (quarkslab.com)
79 points by sprin 1 day ago | hide | 4 comments
74.	
Outburst flood at 1920 BCE supports historicity of China’s Great Flood (sciencemag.org)
28 points by diodorus 23 hours ago | hide | 2 comments
75.	
Some news from LWN (lwn.net)
416 points by gghh 3 days ago | hide | 144 comments
76.	
What Apple should tell you when you lose your iPhone (medium.com)
607 points by walterbell 3 days ago | hide | 204 comments
77.	
Ask HN: My infosec auditor rejects open source. What now?
25 points by _ix 16 hours ago | hide | 10 comments
78.	
Analyzing top four flaws in HTTP/2.0 [pdf] (imperva.com)
11 points by TheAuditor 5 hours ago | hide | 1 comment
79.	
20 Brutal Truths All 20-Somethings Need to Hear (inc.com)
10 points by ffggvv 9 hours ago | hide | 2 comments
80.	
Three States and a Plan: The A.I. of F.E.A.R (2006) [pdf] (mit.edu)
65 points by Wlad007 1 day ago | hide | 23 comments
81.	
Airbnb Files to Raise $850M at $30B Valuation (bloomberg.com)
168 points by tekacs 1 day ago | hide | 163 comments
82.	
Kinesis Advantage 2 (kinesis-ergo.com)
116 points by afshinmeh 23 hours ago | hide | 149 comments
83.	
An Isolated Tribe Emerges from the Rain Forest (newyorker.com)
81 points by gk1 1 day ago | hide | 14 comments
84.	
Anti-submarine Warfare: Seek, but shall ye find? (economist.com)
24 points by prostoalex 1 day ago | hide | 16 comments
85.	
Understanding gradient descent (thegreenplace.net)
120 points by ingve 1 day ago | hide | 29 comments
86.	
Do Oil Companies Really Need $4B per Year of Taxpayers’ Money? (nytimes.com)
62 points by JumpCrisscross 1 day ago | hide | 37 comments
87.	
Can NAT traversal be Tor's killer feature? (2014) (github.com)
112 points by networked 1 day ago | hide | 22 comments
88.	
Can Science Breed the Next Secretariat? (nautil.us)
22 points by dnetesn 20 hours ago | hide | 16 comments
89.	
The Mesmerizing Math of a Wind Turbine on Fire (wired.com)
56 points by stared 1 day ago | hide | 27 comments
90.	
FTC to Crack Down on Paid Celebrity Posts That Aren’t Clear Ads (bloomberg.com)
131 points by lnguyen 1 day ago | hide | 110 comments
91.	
How to enable the Linux / Bash subsystem in Windows 10 (walkingrandomly.com)
27 points by sndean 17 hours ago | hide | 4 comments
92.	
Forget Comcast. Here’s a DIY Approach to Internet Access (backchannel.com)
236 points by nreece 2 days ago | hide | 69 comments
93.	
Americans Don't Care About Prison Phone Exploitation, Says FCC Official (vice.com)
99 points by dsr12 19 hours ago | hide | 75 comments
94.	
OpenSSH 7.3 adds “jump” option to allow indirection through SSH bastions hosts (openssh.com)
18 points by gauravphoenix 1 day ago | hide | 1 comment
95.	
Hackers Could Break into Your Monitor to Spy on You and Manipulate Your Pixels (vice.com)
25 points by chmars 17 hours ago | hide | 5 comments
96.	
Resurrecting the Flower Crowns of Old Ukrainian Wedding Photos (atlasobscura.com)
62 points by lermontov 1 day ago | hide | 39 comments
97.	
Blockchain Reaches a Tipping Point (wsj.com)
4 points by ozdave 16 hours ago | hide | discuss
98.	
News Feed FYI: Further Reducing Clickbait in Feed (fb.com)
281 points by frostmatthew 2 days ago | hide | 154 comments
99.	
Melting ice sheet could release frozen Cold War-era waste (agu.org)
11 points by ashitlerferad 1 day ago | hide | discuss
100.	
SteamVR Tracking (steamgames.com)
200 points by Impossible 2 days ago | hide | 50 comments
101.	
Ring oscillators on Silego GreenPAK 4 (whitequark.org)
23 points by jsnell 1 day ago | hide | 1 comment
102.	
Car Thieves Arrested After Using Laptop and Malware to Steal More Than 30 Jeeps (abc13.com)
127 points by Jerry2 1 day ago | hide | 79 comments
103.	
Who’s the First Person in History Whose Name We Know? (2015) (nationalgeographic.com)
178 points by rfreytag 2 days ago | hide | 127 comments
104.	
Revealing Algorithmic Rankers (freedom-to-tinker.com)
27 points by nkurz 1 day ago | hide | 13 comments
105.	
Things I Won’t Work With: Peroxide Peroxides (2014) (sciencemag.org)
211 points by rishabhd 2 days ago | hide | 102 comments
106.	
Show HN: PyHN - Hacker news client for command line (python.org)
75 points by Socketubs 1 day ago | hide | 17 comments
107.	
Demystifying the Secure Enclave Processor [pdf] (blackhat.com)
119 points by taocp 1 day ago | hide | 29 comments
108.	
Leveraging Search Stages with Filters, Sorting, and Search Terms (octopart.com)
22 points by emilyn 1 day ago | hide | discuss
109.	
Hask is not a category (andrej.com)
51 points by snaky 13 hours ago | hide | 47 comments
110.	
Why We Moved from Amazon Web Services to Google Cloud Platform (lugassy.net)
341 points by jganetsk 1 day ago | hide | 237 comments
111.	
Show HN: 1M rows/s from Postgres to Python (magic.io)
285 points by 1st1 2 days ago | hide | 87 comments
112.	
Interactive Dynamic Video (interactivedynamicvideo.com)
378 points by ne0phyte 2 days ago | hide | 51 comments
113.	
Hitchhiker trees: functional, persistent, off-heap sorted maps (github.com)
149 points by hadronzoo 2 days ago | hide | 31 comments
114.	
1700 blogs for hackers (github.com)
63 points by abdelhai 1 day ago | hide | 8 comments
115.	
Pokemon Go banned by Iranian authorities over 'security' (bbc.com)
31 points by LukaAl 1 day ago | hide | 36 comments
116.	
Find a new city (austinkleon.com)
229 points by mantesso 1 day ago | hide | 186 comments
117.	
Salsa20 design [pdf] (yp.to)
55 points by lucb1e 1 day ago | hide | 20 comments
118.	
AMD’s RX 470 GPU debuts with excellent performance for under $200 (extremetech.com)
15 points by doener 17 hours ago | hide | discuss
119.	
A bar owner in the UK has built a Faraday cage to stop customers using phones (sciencealert.com)
19 points by wbsun 18 hours ago | hide | 17 comments
120.	
An ode to Surface 3 (edandersen.com)
16 points by edandersen 16 hours ago | hide | 7 comments