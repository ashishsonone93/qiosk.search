361.	
Microsoft adds five new AI chat bots to Skype (theverge.com)
22 points by jonbaer 2 days ago | hide | 7 comments
362.	
Report: Operating Systems Should Actively Block Pirated Downloads (torrentfreak.com)
6 points by ionised 2 days ago | hide | 5 comments
363.	
How Tensors Advance Human Technology (medium.com)
14 points by pjama 1 day ago | hide | discuss
364.	
Watson correctly diagnoses woman after doctors were stumped (siliconangle.com)
4 points by srunni 1 day ago | hide | discuss
365.	
A notes taking application using React-Native, Redux and Async-Storage-Engine (github.com)
4 points by shoumma 3 days ago | hide | 1 comment
366.	
Sorry, everyone: The future of sex is total apathy (fusion.net)
41 points by imartin2k 3 days ago | hide | 14 comments
367.	
Ask HN: Talented kid: what to do?
23 points by mudil 2 days ago | hide | 17 comments
368.	
Switzerland bores underground (revue.ch)
53 points by Osiris30 3 days ago | hide | 45 comments
369.	
The Archer's Paradox (youtube.com)
5 points by 83457 1 day ago | hide | discuss
370.	
The Sacred, Spherical Cows of Physics (nautil.us)
4 points by dnetesn 3 days ago | hide | discuss
371.	
No major US hurricanes in 11 years. Odds of that? 1-in-2,300 (arstechnica.com)
5 points by shawndumas 1 day ago | hide | discuss
372.	
Pennsylvania to Apply 6% “Netflix Tax” (allflicks.net)
11 points by chewymouse 3 days ago | hide | 2 comments
373.	
People.ai (YC S16) Helps Businesses Manage Sales Teams with Behavioral Analytics (ycombinator.com)
20 points by stvnchn 3 days ago | hide | 5 comments
374.	
Using an API to build a $1,500/month side business in 4 months (medium.com)
80 points by kullar 3 days ago | hide | 59 comments
375.	
DD-WRT Companion: manage DD-WRT routers from Android on the go (rm3l.org)
3 points by h4ck3r3l 3 days ago | hide | discuss
376.	
HTTP/2: Faster and better than HTTP 1.1, but is it more secure? [pdf] (imperva.com)
3 points by cujanovic 3 days ago | hide | discuss
377.	
NASA, by Any Chance, Are You Missing a Space Capsule? (observer.com)
6 points by vanattab 2 days ago | hide | discuss
378.	
Offer YC: Let me rebrand your startup for free
17 points by pixelfeeder 2 days ago | hide | 12 comments
379.	
Europe has the highest online piracy rates (torrentfreak.com)
3 points by open-source-ux 1 day ago | hide | discuss
380.	
Python FAQ: How do I port to Python 3? (eev.ee)
4 points by keithly 1 day ago | hide | discuss
381.	
Python FAQ: Why should I use Python 3? (eev.ee)
4 points by keithly 1 day ago | hide | discuss
382.	
America's pungent corpse flowers are all mysteriously blooming at once (sciencealert.com)
4 points by lkurtz 1 day ago | hide | discuss
383.	
Silicon Valley arrogance (signalvnoise.com)
9 points by ingve 1 day ago | hide | 2 comments
384.	
How to Build a No-Screws, Snap-Together, 3D Printed R/C Car (makezine.com)
3 points by simonpure 1 day ago | hide | discuss
385.	
Windows 10 free upgrade is still available using Windows 7 and 8 product keys (zdnet.com)
3 points by walterbell 1 day ago | hide | discuss
386.	
Show HN: I pinged the Internet (IPv4) and generated a map (imgur.com)
12 points by achillean 2 days ago | hide | 2 comments
387.	
GCC 4.9.4 Released (gnu.org)
3 points by edelsohn 3 days ago | hide | discuss
388.	
Hackers Fool Tesla S’s Autopilot to Hide and Spoof Obstacles (wired.com)
3 points by 5kg 1 day ago | hide | discuss
389.	
A20 – a pain from the past (tue.nl)
8 points by pud 2 days ago | hide | 1 comment
390.	
Are users trying to make developers angry? (exceptionnotfound.net)
31 points by joeyespo 19 hours ago | hide | 46 comments
391.	
Ask HN: Developers – How did you learn to say NO?
57 points by ninja_to_be 2 days ago | hide | 51 comments
392.	
Vaginas are hot property when it comes to tech (thememo.com)
11 points by morehuman 3 days ago | hide | 2 comments
393.	
For Economy, Aging Population Poses Double Whammy (wsj.com)
62 points by Yhippa 2 days ago | hide | 130 comments
394.	
Pi 3 booting part II: Ethernet (raspberrypi.org)
4 points by jsingleton 1 day ago | hide | 1 comment
395.	
New York to bar sex offenders on parole from playing Pokemon Go (reuters.com)
56 points by jastr 3 days ago | hide | 91 comments
396.	
What Is HTTP/2 All About (auth0.com)
6 points by KukicAdnan 1 day ago | hide | discuss
397.	
Stop hazing your potential hires: Hiring at Spreedly (spreedly.com)
13 points by jusben1369 2 days ago | hide | 14 comments
398.	
Show HN: A class to help you optimize your salary like you optimize your code (fearlesssalarynegotiation.com)
5 points by JoshDoody 1 day ago | hide | 1 comment
399.	
The Washington Post will use robots to write stories about the Rio Olympics (recode.net)
3 points by augb 1 day ago | hide | discuss
400.	
Questions to Ask Before You Join a Startup (mediashift.org)
21 points by djjose 3 days ago | hide | 5 comments
401.	
Can Scala have a highly parallel typechecker like Facebook's Hack? (medium.com)
5 points by virtualwhys 2 days ago | hide | 1 comment
402.	
Facebook should rewrite Headlines instead of banning them (vox.com)
5 points by abhi3 2 days ago | hide | 3 comments
403.	
Megaupload 2.0 Will Link File Transfers to Bitcoin Transactions (torrentfreak.com)
6 points by chewymouse 1 day ago | hide | discuss
404.	
Customer.io Actions – Why did we do it this way? (customer.io)
16 points by kerry_pd 2 days ago | hide | discuss
405.	
Moon Express becomes first private company to get US approval for lunar mission (theverge.com)
14 points by ourmandave 4 days ago | hide | discuss
406.	
Ask HN: Grokking concurrent applications?
7 points by tastyface 2 days ago | hide | 2 comments
407.	
Writing a Ray Tracer in Go (markphelps.me)
6 points by dsr12 2 days ago | hide | discuss
408.	
Introducing React Native Ubuntu (ubuntu.com)
4 points by daker 2 days ago | hide | 1 comment
409.	
Health Secrets of the Amish (nytimes.com)
11 points by davidf18 2 days ago | hide | 5 comments
410.	
Yahoo’s Marissa Mayer on Selling a Company While Trying to Turn It Around (bloomberg.com)
68 points by terryauerbach 2 days ago | hide | 128 comments
411.	
Machine Vision’s Achilles’ Heel Revealed by Google Brain Researchers (technologyreview.com)
9 points by mathattack 2 days ago | hide | discuss
412.	
Climate Scientists Say the Siberian Anthrax Outbreak Is a Sign of What’s to Come (psmag.com)
5 points by Osiris30 2 days ago | hide | discuss
413.	
RDS MySQL storage dropped from 450GB to 0 in 10min. Ok after reboot. Ideas? (twitter.com)
6 points by velmu 2 days ago | hide | discuss
414.	
Ask HN: Are you interested in picking up “idea sunday” threads again?
16 points by azeirah 2 days ago | hide | 12 comments
415.	
My 30 Years at PCMag, Part One (pcmag.com)
10 points by smegel 3 days ago | hide | 3 comments
416.	
Configuration (mis)management or why I hate puppet, ansible, salt, etc. (scriptcrafty.com)
43 points by jtrtoo 1 day ago | hide | 45 comments
417.	
The Ekalavya Effect: A Parable for Internet Learning (medium.com)
4 points by selvan 2 days ago | hide | discuss
418.	
Introducing Guesstimate, a Spreadsheet for Things That Aren’t Certain (medium.com)
4 points by mpweiher 2 days ago | hide | 1 comment
419.	
Intel recalls Basis Peak watches (semiaccurate.com)
7 points by dsr_ 2 days ago | hide | 1 comment
420.	
Linkedin: Open Sourcing Test Butler (linkedin.com)
10 points by samber 2 days ago | hide | discuss
421.	
78% of Children with ADD No Longer Have It as Adults (theconversation.com)
12 points by salmonet 3 days ago | hide | 4 comments
422.	
The Best Code I’ve Ever Written (medium.com)
9 points by kiyanwang 3 days ago | hide | 4 comments
423.	
Severe vulnerabilities discovered in HTTP/2 protocol (zdnet.com)
10 points by GordonS 3 days ago | hide | 1 comment
424.	
Announcing Tokio a Finagle inspired network application framework for Rust (medium.com)
24 points by steveklabnik 3 days ago | hide | 1 comment
425.	
Comcast wants to charge broadband users more to not sell their browsing history (dslreports.com)
10 points by doctorshady 3 days ago | hide | 1 comment
426.	
Ask HN: Is anyone interested in a PDF rendering service?
11 points by danioso 3 days ago | hide | 4 comments
427.	
2015 Set a Frenzy of Climate Records (scientificamerican.com)
48 points by philipalexander 3 days ago | hide | 43 comments
428.	
Programming Languages as Boy Scouts (willcrichton.net)
53 points by wcrichton 3 days ago | hide | 84 comments
429.	
Your Credit Card Company Is Selling Your Purchase Data to Online Advertisers (businessinsider.com)
7 points by nstj 3 days ago | hide | discuss
430.	
Ask HN: Does anyone have any inside info on Visa and MasterCard engineering?
9 points by jorgecastillo 3 days ago | hide | 3 comments
431.	
MSFT Pitches Technology That Can Read Facial Expressions at Political Rallies (theintercept.com)
4 points by pkaeding 2 days ago | hide | discuss
432.	
Kinesis Ergo Keyboard Advantage 2 Available
8 points by dmbaggett 3 days ago | hide | 4 comments
433.	
How I Solved Instagram’s Hashtag Problem (medium.com)
3 points by rahulchowdhury 2 days ago | hide | 1 comment
434.	
How to prepare your fresh Mac for software development (medium.com)
27 points by mtkocak 3 days ago | hide | 36 comments
435.	
What's Up with the British? (linkedin.com)
4 points by vool 3 days ago | hide | discuss
436.	
Ask HN: Remember FidoNet?
15 points by bjourne 3 days ago | hide | 3 comments
437.	
Amish Barn Dust May Prevent Asthma in Children (nytimes.com)
7 points by giardini 3 days ago | hide | discuss
438.	
Comcast supports higher prices for customers who want Web privacy (arstechnica.com)
4 points by om42 3 days ago | hide | discuss
439.	
Programming with Japanese Candies [video] (youtube.com)
4 points by Halienja 3 days ago | hide | discuss
440.	
New 1Password subscription service (agilebits.com)
3 points by EwanToo 3 days ago | hide | discuss
441.	
CryEngine bugs (viva64.com)
20 points by AndreyKarpov 3 days ago | hide | 3 comments
442.	
Defending Privacy at the U.S. Border – for Travelers with Digital Devices (2011) (eff.org)
45 points by neverminder 4 days ago | hide | 8 comments
443.	
New attack steals SSNs, e-mail addresses, and more from HTTPS pages (arstechnica.com)
13 points by Aelinsaar 3 days ago | hide | 4 comments
444.	
Criticizing the Rust Language, and Why C/C++ Will Never Die (2015) (viva64.com)
38 points by federicoponzi 1 day ago | hide | 48 comments
445.	
In a new video, Russian engineers outline a daring plan to land on Ganymede (arstechnica.com)
4 points by sjayasinghe 3 days ago | hide | discuss
446.	
Will human sexuality ever be free from stone age, evolutionary impulses? (aeon.co)
4 points by jseliger 3 days ago | hide | 3 comments
447.	
Why I Love RSS and You Do Too (inessential.com)
7 points by type0 3 days ago | hide | discuss
448.	
Flex your computing power! (suchflex.com)
53 points by suchflex 3 days ago | hide | 79 comments
449.	
Ask HN: Why are sites now breaking login forms into stages (name then password)?
51 points by microman 2 days ago | hide | 50 comments
450.	
New York bill would require some game devs to track where sex offenders live (polygon.com)
3 points by AWildDHHAppears 3 days ago | hide | discuss
451.	
AppStore Developer Payout Hits $50B, July Sets Record, Biggest Month Ever (macrumors.com)
3 points by heshamg 3 days ago | hide | 1 comment
452.	
Wanna have a tantrum and smash something? Be her guest (marketplace.org)
3 points by apress 3 days ago | hide | discuss
453.	
Moving to GitLab Yes it's worth it (akitaonrails.com)
10 points by jobvandervoort 3 days ago | hide | 1 comment
454.	
LastPass Authenticator with push notifications (lastpass.com)
5 points by chrisan 3 days ago | hide | discuss
455.	
75 Percent of Americans Say They Eat Healthy – Despite Evidence to the Contrary (npr.org)
4 points by evo_9 3 days ago | hide | discuss
456.	
Show HN: New Cool Calendar App for Android Just Launched (venturebeat.com)
44 points by petermolyneux 3 days ago | hide | 8 comments
457.	
The Crockford Keyboard (1981) (crockford.com)
4 points by adgasf 3 days ago | hide | discuss
458.	
“Culture fit” craze is turning tech from innovators to ideological fiefdoms (imgur.com)
58 points by emblem21 2 days ago | hide | 59 comments
459.	
LibreOffice under the hood: a year of progress from 5.0 to 5.2 (gnome.org)
8 points by ashitlerferad 4 days ago | hide | discuss
460.	
Anniversary Update hides programs, forces Skype on users (osnews.com)
5 points by turrini 4 days ago | hide | discuss
461.	
The Case Against the Mortgage Interest Deduction (priceonomics.com)
43 points by tnorthcutt 3 days ago | hide | 68 comments
462.	
Can mythbusters like Snopes.com keep up in a post-truth era? (theguardian.com)
57 points by ca98am79 3 days ago | hide | 77 comments
463.	
British woman held after being seen reading book about Syria on plane (theguardian.com)
56 points by azuajef 2 days ago | hide | 78 comments
464.	
Is the Elite Media Failing to Reach Trump Voters? (slate.com)
4 points by 3eto 3 days ago | hide | discuss
465.	
Ask HN: Does HN have any plans to build in notifications?
7 points by Kinnard 3 days ago | hide | 5 comments
466.	
[dupe] Bitcoin not money, judge rules in victory for backers (phys.org)
19 points by dnetesn 21 hours ago | hide | 7 comments
467.	
Nervous about nukes again? Here’s what you need to know about the Button (washingtonpost.com)
45 points by mysterypie 3 days ago | hide | 67 comments
468.	
Calculus at the heart of the STEM gender gap, study suggests (huffingtonpost.com)
38 points by ivan_ah 2 days ago | hide | 60 comments
469.	
[dupe] Fractal Tree to create fast, snapshottable, massively scalable databases (github.com)
19 points by jgrodziski 22 hours ago | hide | 5 comments
470.	
Apple makes slight progress on diversity while rivals make practically none (washingtonpost.com)
20 points by clifanatic 3 days ago | hide | 43 comments
471.	
[dupe] U.S. Team Wins First Place at International Math Olympiad (wordplay.blogs.nytimes.com)
319 points by vinchuco 3 days ago | hide | 221 comments
472.	
Ask HN: Any experience with defiant children?
35 points by rymohr 3 days ago | hide | 56 comments
473.	
[dupe] Stranger Things: meet the design genius behind TV's most talked about title font (telegraph.co.uk)
10 points by smacktoward 22 hours ago | hide | 4 comments
474.	
Ask HN: What's the best investment you've made?
44 points by cmod 3 days ago | hide | 81 comments
475.	
[dupe] Missouri Governor Jay Nixon Gets Ordered to Serve as a Public Defender (theatlantic.com)
53 points by kposehn 2 days ago | hide | 3 comments
476.	
C++ exception handling mistakes and how to avoid them (acodersjourney.com)
14 points by debh 3 days ago | hide | 3 comments