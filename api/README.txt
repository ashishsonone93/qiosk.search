This is the code for search api server

- - - - - - - - - - - -
To run:
  node index.js

- - - - - - - - - - - - 
Now can access search api like the following

Search Endpoints:
GET /users?q=Rambo&limit=10
GET /articles?q=Facebook acquires
GET /tags?q=Technology
GET /collections?q=Elasticsearch&limit=15

Each search endpoint takes two query paramters:
  q : search phrase
  limit : number of results to return
  
- - - - - - - 

Get item by id Endpoints:
Use this to test/check if an item(or its updated version) has been ingested into ES or not

GET /users/<id>
GET /articles/<id>
GET /tags/<id>
GET /collections/<id>