var express = require('express');
var cors = require('cors');
var app = express();

var searchApi = require('./search_api');
var config = require('./config');
var PORT = config.PORT || 8004;
console.log("%j %j %j", config.ES_SERVER, config.ES_INDEX, config.API_SERVER);

app.use(cors());
app.use('/', searchApi.router(config.ES_SERVER, config.ES_INDEX));

app.listen(PORT);
console.log("search api server listening on localhost:%j", PORT);