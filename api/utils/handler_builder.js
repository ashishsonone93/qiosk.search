var elasticsearch = require('elasticsearch');
var queryBuilder = require('./query_builder');
var utils = require('./utils');

function handlerBuilder(esServer, esIndex){
  //this client is used by all subsequent handlers
  const client = new elasticsearch.Client({
    host: esServer,
  });

  //return 'item by id' request handler for particular entity type
  var buildIdRequestHandler = function(type){
    var handler = function(req, res){
      var id = req.params.id;
      var promise = client.get({
        index: esIndex,
        type: type,
        id: id
      });
      
      //process the updatedAt ts if found
      promise = promise.then(function(result){
        //console.log("buildIdRequestHandler success %j", result);

        if(result && result._source){
          var item = utils.searchHitToResult(type)(result);
          return res.json(item);
        }
        
        res.status(404);
        res.json({
          error : 'NOT_FOUND'
        });
      });
      
      promise.catch(function(err){
        //console.log("buildIdRequestHandler error %j", err);
        if(err && err.status == 404){
          res.status(404);
          res.json({
            error: 'NOT_FOUND'
          });
        }
        else{
          res.status(500);
          res.json({
            error : err
          });
        }
      });
    }
    return handler;
  }
  
  //execute the search on ES given the type and query payload
  var search = function(type, query){
    var promise = client.search({
      index: esIndex,
      type: type,
      body: query
    });

    promise = promise.then(function(result){
      var r = {
        total : result.hits.total,
        results : result.hits.hits.map(utils.searchHitToResult(type))
      };
      return r;
    });

    return promise;
  }

   //return 'search by phrase' request handler for a particular entity type
  var buildSearchRequestHandler = function(type, fields, script, maxBoost){
    var handler = function(req, res, next){
      var phrase = req.query.q;
      
      var start = req.query.start || 0;
      var limit = req.query.limit || 10;
      limit = parseInt(limit);

      var qBody = queryBuilder.buildQuery(phrase, limit, fields, script, maxBoost, start);

      if (req.query.isInfluencer) {
        qBody.query.bool.filter.bool["must"] = [
          {
            "term" : {"isInfluencer" : 1}
          }
        ];
      }
      
      //console.log("qBody %j", qBody);
      var promise = search(type, qBody);

      promise = promise.then(function(result){
        console.log("%s (success) (%s) phrase=%j, limit=%j, #results=%j", new Date().toISOString(), type, phrase, limit, result.results.length);
        
        req.result = result;
        req.error = null;
        next();
      });

      promise.catch(function(err){
        console.log("%s (error) (%s) phrase=%j, limit=%j err=%j", new Date().toISOString(), type, phrase, limit, err);
        
        req.result = null;
        req.error = {
          status : 500,
          json : err
        };
        next();
      });
    };
    
    return handler;
  }

  var countP = function(type, query){
    var promise = client.count({
      index: esIndex,
      type: type,
      body: query
    });

    promise = promise.then(function(result){
      return result.count;
    });

    return promise;
  }
  
  var buildCountPromise= function(type, phrase, fields, isInfluencer=false){
    var qBody = queryBuilder.buildCountQuery(phrase, fields);

    if (isInfluencer) {
      qBody.query.bool.filter.bool["must"] = [
        {
          "term" : {"isInfluencer" : 1}
        }
      ];
    }

    //console.log('count query for type %j is = %j', qBody);
    //console.log("qBody %j", qBody);
    var promise = countP(type, qBody);
    return promise;
  }

  //assume req.result (set if success), 
  //other wise req.error.status and req.error.json will be set
  var nextHandler = function(req, res){
    if(req.result){
      res.json(req.result);
    }
    else if(req.error){
      res.status(req.error.status || 500);
      res.json(req.error.json || {error : "UNKNOWN"});
    }
    else{
      res.status(500);
      res.json({error : "UNKNOWN"});
    }
  }
  
  return {
    buildIdRequestHandler : buildIdRequestHandler,
    buildSearchRequestHandler : buildSearchRequestHandler,
    buildCountPromise : buildCountPromise,
    nextHandler : nextHandler
  }
}

module.exports = handlerBuilder;