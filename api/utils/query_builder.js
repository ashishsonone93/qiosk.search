function buildQuery(phrase, limit, fields, script, maxBoost, start){
  var query = {
      "from" : start,
      "size" : limit,
      "query" : {
          "bool" : {
              "must" : {
                  "multi_match" : {
                      "type" : "most_fields",
                      "query" : phrase,
                      "fields" : fields
                  }
              },
              "filter" : {
                  "bool" : {
                      "must_not" : [
                          {
                              "term" : {"isDeleted" : true}
                          }
                      ]
                  }
              }
          }
      },
      "rescore" : {
          "window_size" : limit*2,
          "query" : {
              "query_weight" : 1,
              "rescore_query_weight" : 1,
              "rescore_query" : {
                  "function_score" : {
                      "functions" : [
                          {
                              "script_score" : {
                                  "script" : {
                                      "inline" : script,
                                      "lang" : "expression"
                                  }
                              }
                          }
                      ],
                      "score_mode" : "sum",
                      "boost_mode": "multiply",
                      "max_boost" : maxBoost
                  }
              }
          }
      }
   };

   if (!phrase){
     delete query.query.bool.must;
   }

   return query;
}

function buildCountQuery(phrase, fields){
  var query = {
      "query" : {
          "bool" : {
              "must" : {
                  "multi_match" : {
                      "type" : "most_fields",
                      "query" : phrase,
                      "fields" : fields
                  }
              },
              "filter" : {
                  "bool" : {
                      "must_not" : [
                          {
                              "term" : {"isDeleted" : true}
                          }
                      ]
                  }
              }
          }
      }
   };
  
   if (!phrase){
     delete query.query.bool.must;
   }
  
   return query;
}

module.exports = {
  buildQuery : buildQuery,
  buildCountQuery : buildCountQuery
};