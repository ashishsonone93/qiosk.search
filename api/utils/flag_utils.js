var RSVP = require('rsvp');
var config = require('../config');
var rp = require('request-promise');

function getUserFlags(userId, userList){
  var toCheckIds = userList.map(function(e){return e._id});
  var body = {
    userId : userId,
    toCheckIds : toCheckIds
  };
  
  console.log("getUserFlags | body %j | url %j", body, config.API_SERVER + "/follow/getIsFollowingFlagWithUserids");
  
  var promise = rp({
    url: config.API_SERVER + "/follow/getIsFollowingFlagWithUserids",
    body: body,
    method: 'POST',
    headers: {
      'apikey': 'baby@qiosk',
      'Content-Type' : 'application/json'  
    },
    json: true
  });
  
  promise = promise.then(function(flagMap){
    console.log("getUserFlags %j %j", userId, flagMap);
    var newUserList = userList.map(function(e){
      e.isFollowing = flagMap[e._id];
      return e;
    });
    return newUserList;
  });
  return promise;
}

function getArticleFlags(userId, objectList){
  var toCheckIds = objectList.map(function(e){return e._id});
  
  var body = {
    userId : userId,
    toCheckArticleIds : toCheckIds
  };
  
  console.log("getArticleFlags %j", body);
  
  var promise = rp({
    url: config.API_SERVER + "/article/checkArticleSavedInMyCollection",
    body: body,
    method: 'POST',
    headers: {
      'apikey': 'baby@qiosk',
      'Content-Type' : 'application/json'  
    },
    json: true
  });
  
  promise = promise.then(function(flagMap){
    console.log("getArticleFlags %j %j", userId, flagMap);
    var newList = objectList.map(function(e){
      e.isSaved = flagMap[e._id];
      return e;
    });
    return newList;
  });
  return promise;
}

module.exports = {
  getUserFlags : getUserFlags,
  getArticleFlags : getArticleFlags
};