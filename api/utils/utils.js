function searchHitToResultHelper(type, hit){
  var item = hit._source;
  item._score = hit._score;

  //rename field id to _id just like mongodb
  item._id = item.id;
  delete item.id;

  //todo undo type conversions done while ingesting (e.g bool to long)
  //todo remove extra fields (e.g #likes, #comments)
  if(type == 'user'){
    item.isInfluencer = item.isInfluencer == 1 ? true : false;
    delete item['#followers'];
  }
  else if(type == 'tag'){
    //delete item['#articles'];   
  }
  else if(type == 'article'){
    delete item['#likes'];
    delete item['#notes'];
    delete item['#shares'];
  }
  else if(type == 'collection'){
    delete item['#likes'];
    delete item['#notes'];
    delete item['#shares'];
  }
  
  return item;
}

function searchHitToResult(type){
  return function(hit){
    return searchHitToResultHelper(type, hit);
  }
}

module.exports = {
  searchHitToResult : searchHitToResult
};