var express = require('express');
var flagUtils = require('./utils/flag_utils');
var RSVP = require('rsvp');

function router(esServer, esIndex){
  var handlerBuilder = require('./utils/handler_builder');
  //init handlerBuilder with server url and index name
  var handlerBuilder = handlerBuilder(esServer, esIndex);
  
  var router = express.Router();
  
  router.get('/test', function(req, res){
    var userId = "5884ef3d9cd8846861d2c872";
    var objectList = [{_id : "58825038170f2a75e9c11efc" }, {_id : "5884e2ec9cd8846861d2c85f"}, {_id : "58821de4e7b7794aa3d9d262"}];
    
    var promise = flagUtils.getUserFlags(userId, objectList);
    promise = promise.then(function(list){
      res.json({
        results : list
      });
    });

    promise.catch(function(err){
      res.status(500);
      res.json(err);
    });
    
  });
  
  /*
    routes to get items by id
  */
  router.get('/articles/:id', handlerBuilder.buildIdRequestHandler('article'));
  router.get('/tags/:id', handlerBuilder.buildIdRequestHandler('tag'));
  router.get('/collections/:id', handlerBuilder.buildIdRequestHandler('collection'));
  router.get('/users/:id', handlerBuilder.buildIdRequestHandler('user'));
  
  /*
    Search articles
    required params:
      q : query string
    optional params:
      limit : (default 10)
  */
  var A_FIELDS = ["title.article_english^2", "title.article_autocomplete", "content.article_english", "content.article_autocomplete"];
  var A_SCRIPT = "log10(1 + doc['#likes'] + doc['#notes'] + doc['#shares'])/12";
  var A_MAX_BOOST = 0.25;
  
  router.get('/articles/', handlerBuilder.buildSearchRequestHandler('article', A_FIELDS, A_SCRIPT, A_MAX_BOOST), function(req, res){
    if(req.result){
      var userId = req.query.userId; //get userId from query parameter
      
      var total = req.result.total;
      var objectList = req.result.results; //result from buildSearchRequestHandler
      
      if((!userId || !objectList || objectList.length == 0)){
        var promise = RSVP.resolve(objectList); //return list as is
      }
      else{
        var promise = flagUtils.getArticleFlags(userId, objectList);
      }
      promise = promise.then(function(list){
        res.json({
          total : total,
          results : list
        });
      });
      
      promise.catch(function(err){
        res.status(500);
        res.json(err);
      });
    }
    else if(req.error){
      res.status(req.error.status || 500);
      res.json(req.error.json || {error : "UNKNOWN"});
    }
    else{
      res.status(500);
      res.json({error : "UNKNOWN"});
    }
  });
  
  
  /*
    Search collections
    required params:
      q : query string
    optional params:
      limit : (default 10)
  */
  var C_SCRIPT = "log10(1 + doc['#likes'] + doc['#notes'] + doc['#shares'])/12";
  var C_FIELDS = ["name.collection_english^2", "name.collection_autocomplete", "description.collection_english", "description.collection_autocomplete"];
  var C_MAX_BOOST = 0.25;
  
  router.get('/collections/', handlerBuilder.buildSearchRequestHandler('collection', C_FIELDS, C_SCRIPT, C_MAX_BOOST), handlerBuilder.nextHandler);
  
  /*
    Search tags
    required params:
      q : query string
    optional params:
      limit : (default 10)
  */
  var T_SCRIPT = "log10(1 + doc['#articles'].value)/12";
  var T_FIELDS = ["tagName.tag_english^2", "tagName.tag_autocomplete", "tagDescription.tag_english", "tagDescription.tag_autocomplete"];
  var T_MAX_BOOST = 0.25;
  router.get('/tags/', handlerBuilder.buildSearchRequestHandler('tag', T_FIELDS, T_SCRIPT, T_MAX_BOOST), handlerBuilder.nextHandler);
  
  /*
    Search users
    required params:
      q : query string
    optional params:
      limit : (default 10)
  */
  var U_SCRIPT = "log10(1 + doc['#followers'].value + doc['isInfluencer'].value*300)/12";
  var U_FIELDS = ["name.user_raw^2", "name.user_autocomplete", "description.user_english", "description.user_autocomplete", "tagLine.user_english", "tagLine.user_autocomplete",];
  var U_MAX_BOOST = 0.25;
  router.get('/users/', handlerBuilder.buildSearchRequestHandler('user', U_FIELDS, U_SCRIPT, U_MAX_BOOST), function(req, res){
    if(req.result){
      var userId = req.query.userId; //get userId from query parameter
      
      var total = req.result.total;
      var objectList = req.result.results; //result from buildSearchRequestHandler
      
      if((!userId || !objectList || objectList.length == 0)){
        var promise = RSVP.resolve(objectList); //return list as is
      }
      else{
        var promise = flagUtils.getUserFlags(userId, objectList);
      }
      promise = promise.then(function(list){
        res.json({
          total : total,
          results : list
        });
      });
      
      promise.catch(function(err){
        res.status(500);
        res.json(err);
      });
    }
    else if(req.error){
      res.status(req.error.status || 500);
      res.json(req.error.json || {error : "UNKNOWN"});
    }
    else{
      res.status(500);
      res.json({error : "UNKNOWN"});
    }
  });
  
  //brand new function to return count of all types (articles, collections, tags, users, influencers)
  router.get('/all/', function(req, res){
    var phrase = req.query.q;
    var p1 = handlerBuilder.buildCountPromise('article', phrase, A_FIELDS);
    var p2 = handlerBuilder.buildCountPromise('collection', phrase, C_FIELDS);
    var p3 = handlerBuilder.buildCountPromise('tag', phrase, T_FIELDS);
    var p4 = handlerBuilder.buildCountPromise('user', phrase, U_FIELDS);
    var p5 = handlerBuilder.buildCountPromise('user', phrase, U_FIELDS, true);

    var allPromises = {
      articles : p1,
      collections : p2,
      tags : p3,
      users : p4,
      influencers : p5
    };

    var promise = RSVP.hash(allPromises);

    promise = promise.then(function(result){
      console.log("%s /all/ (success) phrase=%j", new Date().toISOString(), phrase);

      res.json(result);
    });

    promise.catch(function(err){
      console.log("%s /all/ (error) phrase=%j, err=%j", new Date().toISOString(), phrase, err);
      res.status(500);
      res.json(err);
    });

  });

  return router;
}

module.exports = {
  router : router
};